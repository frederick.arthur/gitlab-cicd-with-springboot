package com.alpha.gitlabcicddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCicdDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCicdDemoApplication.class, args);
	}

}
