#
# Build stage
#
FROM maven:3.8.7-amazoncorretto-17 AS build
WORKDIR /home/app
COPY src src
COPY pom.xml .
RUN mvn clean package

EXPOSE 8080
ENTRYPOINT ["java","-jar","target/gitlab-cicd-demo-0.0.1-SNAPSHOT.jar"]